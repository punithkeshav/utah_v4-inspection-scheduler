/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspection_Scheduler_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_Scheduler_V5_2_PageObjects.Inspection_Scheduler_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Inspection Scheduler - Alternate Scenario 1",
        createNewBrowserInstance = false
)

public class FR1_Capture_Inspection_Scheduler_AlternateScenario1 extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Inspection_Scheduler_AlternateScenario1()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }
    public TestResult executeTest()
    {
        if (!Navigate_To_Inspection_Scheduler())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Inspection_Scheduler())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Inspection Scheduler record is saved.");
    }

    public boolean Navigate_To_Inspection_Scheduler()
    {
      

        //Inspection Scheduler
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Module()))
        {
            error = "Failed to wait for 'Inspection Scheduler' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Module()))
        {
            error = "Failed to click on 'Inspection Scheduler' module";
            return false;
        }
        SeleniumDriverInstance.pause(4000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Inspection Scheduler' module search page.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(3000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean Capture_Inspection_Scheduler()
    {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

   
//New Business Unit

  if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Dropdown()))
        {
            error = "Failed to wait for 'Business Unit' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Dropdown()))
        {
            error = "Failed to click Business Unit dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.businessUnitSearch()))
        {
            error = "Failed to enter Business Unit option :" + getData("Business Unit");
            return false;
        }
		

      pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.Text2(getData("Business Unit"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business Unit");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.Text2(getData("Business Unit"))))
        {
            error = "Failed to click Business Unit drop down option : " + getData("Business Unit");
            return false;
        }
     narrator.stepPassedWithScreenShot("Business Unit " + getData("Business Unit") + " -> " + getData("Business Unit") + " -> " + getData("Business Unit"));

 //New Functional Location
   if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.functional_Dropdown()))
        {
            error = "Failed to wait for 'Functional location' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.functional_Dropdown()))
        {
            error = "Failed to click Functional location dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.functionalSearch()))
        {
            error = "Failed to enter Functional location option :" + getData("Functional location");
            return false;
        }
		

      pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.Text2(getData("Functional location"))))
        {
            error = "Failed to wait for Functional location drop down option : " + getData("Functional location");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.Text2(getData("Functional location"))))
        {
            error = "Failed to click Functional location drop down option : " + getData("Functional location");
            return false;
        }
		 narrator.stepPassedWithScreenShot("Functional location " + getData("Functional location") + " -> " + getData("Functional location") + " -> " + getData("Functional location"));

//Inspection Type
  if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.inspectionType_Dropdown()))
        {
            error = "Failed to wait for 'Inspection type' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.inspectionType_Dropdown()))
        {
            error = "Failed to click Inspection type dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.inspectionSearch1()))
        {
            error = "Failed to enter Inspection type option :" + getData("Inspection type");
            return false;
        }
		

      pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.Text2(getData("Inspection type"))))
        {
            error = "Failed to wait for Inspection type drop down option : " + getData("Inspection type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.Text2(getData("Inspection type"))))
        {
            error = "Failed to click Inspection type drop down option : " + getData("Inspection type");
            return false;
        }
narrator.stepPassedWithScreenShot("Inspection type " + getData("Inspection type") + " -> " + getData("Inspection type") + " -> " + getData("Inspection type"));
   
//Checklists to be done
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.checklist_Dropdown()))
        {
            error = "Failed to wait for Inspections location dropdown.";
            return false;
        }
        
         if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.checklist_Dropdown()))
        {
            error = "Failed to wait for Inspections location dropdown.";
            return false;
        }
         
         pause(2000);
         
          if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.checklist_Search()))
        {
            error = "Failed to wait for Inspections location dropdown.";
            return false;
        }

          pause(5000);
          if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.checklistSelect1()))
        {
            error = "Failed to click the Impact type option: " + getData("Checklists to be done");
            return false;
        }
          
         if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.dropClick()))
        {
            error = "Failed to click the Impact type option: " + getData("Checklists to be done");
            return false;
        }
        
     
        narrator.stepPassedWithScreenShot("Checklists to be done: " + getData("Checklists to be done") + " -> " + getData("Checklists to be done") + " -> " + getData("Checklists to be done"));

  if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.control_Dropdown()))
        {
            error = "Failed to wait for 'Control' dropdown..";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.control_Dropdown()))
        {
            error = "Failed to click Control dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.control_Search()))
        {
            error = "Failed to enter Control option :" + getData("Control");
            return false;
        }
	    pause(2000);
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.control_Search(),getData("Control")))
        {
            error = "Failed to click the Control option: " + getData("Control");
            return false;
        }
          
         if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
            pause(3000);	

      pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.Text2(getData("Control"))))
        {
            error = "Failed to wait for Control drop down option : " + getData("Control");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.Text2(getData("Control"))))
        {
            error = "Failed to click Control drop down option : " + getData("Control");
            return false;
        }
		 narrator.stepPassedWithScreenShot("Control " + getData("Control") + " -> " + getData("Control") + " -> " + getData("Control"));

//new control
pause(3000);
        //Name of inspection
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.nameOfInspectionField()))
        {
            error = "Failed to wait for name of Inspection Field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.nameOfInspectionField()))
        {
            error = "Failed to click the name of Inspection Field.";
            return false;
        }
         if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.nameOfInspectionField(), getData("Name of inspection")))
      
            {
                error = "Failed to enter Name of inspection option :" + getData("Name of inspection");
                return false;
            }
        narrator.stepPassedWithScreenShot("Successfully entered Name of inspection.");


        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.SaveButton()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.SaveButton()))
        {
            error = "Failed to click on Save Button";
            return false;
        }

        pause(3000);
        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Inspection_Scheduler_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Inspection_Scheduler_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }
pause(3500);
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.validateSave());
       //String recordNumber = SeleniumDriverInstance.retrieveAttributeByXpath(Inspection_Scheduler_PageObjects.recordSavedNumber());
        if (!SaveFloat.equals("Record saved"))
        {
            //narrator.stepPassedWithScreenShot("Failed to save record.");
            // narrator.stepPassedWithScreenShot("Record saved."+ recordNumber + "");
            //return false;
        }
        //narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
            
        return true;
    }

}
