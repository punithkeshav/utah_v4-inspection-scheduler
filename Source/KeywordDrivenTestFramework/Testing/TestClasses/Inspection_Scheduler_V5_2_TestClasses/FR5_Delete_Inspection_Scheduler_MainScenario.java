/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspection_Scheduler_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_Scheduler_V5_2_PageObjects.Inspection_Scheduler_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "FR5_Delete_Inspection_Scheduler_MainScenario",
        createNewBrowserInstance = false
)

public class FR5_Delete_Inspection_Scheduler_MainScenario extends BaseClass
{

    String error = "";

    public FR5_Delete_Inspection_Scheduler_MainScenario()
    {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (! Navigate_To_Inspection_Scheduler())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        if (!delete_InspectionScheduler())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Inspection Scheduler record deleted");
    }
    
public boolean Navigate_To_Inspection_Scheduler()
    {
      

        //Inspection Scheduler
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Module()))
        {
            error = "Failed to wait for 'Inspection Scheduler' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Module()))
        {
            error = "Failed to click on 'Inspection Scheduler' module";
            return false;
        }
        SeleniumDriverInstance.pause(4000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Inspection Scheduler' module search page.");

        return true;
    }

    public boolean delete_InspectionScheduler()
    {
        pause(10000);

     //Click on search
         if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.searchButton()))
        {
            error = "Failed to click on 'search' button.";
            return false;
        }

       if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.searchButton()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }
          
          narrator.stepPassedWithScreenShot("Successfully click 'search' button.");
          
          pause(10000);
          
          //Select record
          
      if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.clickRecord()))
        {
            error = "Failed to click on 'record'.";
            return false;
        }

       if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.clickRecord()))
        {
            error = "Failed to click on 'record' .";
            return false;
        }
          
          narrator.stepPassedWithScreenShot("Successfully click 'on the record' .");
          
          pause(10000);
          
            //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
          //navigate to to delete button
          
           if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.deleteButton()))
        {
            error = "Failed to wait for 'delete button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.deleteButton()))
        {
            error = "Failed to click on 'delete button";
            return false;
        }
       
        narrator.stepPassedWithScreenShot("Successfully clicked delete button.");
        pause(5000);
        
       // SeleniumDriverInstance.switchToFrameByXpath(Inspection_Scheduler_PageObjects.popUp());
        SeleniumDriverInstance.switchToTabOrWindow();
        //Are you sure you want to delete this record?
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.deletePopUpButton()))
        {
            error = "Failed to wait for 'delete popup";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.deletePopUpButton()))
        {
            error = "Failed to click on 'delete popup";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked confirmation to delete button.");
        pause(6000);
       
        //Delete Popup confirmation
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.deletConButton()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.deletConButton()))
        {
            error = "Failed to click on Save Button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save and Close");
        
        pause(15000);

return true;

}

}