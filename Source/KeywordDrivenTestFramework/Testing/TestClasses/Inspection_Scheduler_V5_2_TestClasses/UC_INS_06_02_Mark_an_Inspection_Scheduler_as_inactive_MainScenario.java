/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspection_Scheduler_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_Scheduler_V5_2_PageObjects.Inspection_Scheduler_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */

  
@KeywordAnnotation(
        Keyword = "UC_INS0602_MarkAnInspectionScheduler_Inactive_MainScenario",
        createNewBrowserInstance = false
)

public class UC_INS_06_02_Mark_an_Inspection_Scheduler_as_inactive_MainScenario extends BaseClass
{

    String error = "";

    public UC_INS_06_02_Mark_an_Inspection_Scheduler_as_inactive_MainScenario()
    {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }
 public TestResult executeTest()
    {
        if (!Navigate_To_Inspection_Scheduler())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        if (!Update_ActiveToInactive())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("•	Inspection Scheduler updates saved ");
    }
    
    public boolean Navigate_To_Inspection_Scheduler()
    {
      

        //Inspection Scheduler
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Module()))
        {
            error = "Failed to wait for 'Inspection Scheduler' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_Module()))
        {
            error = "Failed to click on 'Inspection Scheduler' module";
            return false;
        }
        SeleniumDriverInstance.pause(4000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Inspection Scheduler' module search page.");

        //Show All
       if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.showAllFilter()))
        {
            error = "Failed to wait for 'Show all' button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.showAllFilter()))
        {
            error = "Failed to click on 'Show all' button";
            return false;
        }
        SeleniumDriverInstance.pause(4000);
        narrator.stepPassedWithScreenShot("Successfully navigated to filter page.");
        
       
        

     
        return true;
    }

    public boolean Update_ActiveToInactive()
    {
        pause(5000);
        //Select only active records
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.activeInactiveDD()))
        {
            error = "Failed to wait for 'Inspection Scheduler' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.activeInactiveDD()))
        {
            error = "Failed to click on 'Inspection Scheduler' module";
            return false;
        }
       
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Active/Inaction dropdown.");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.activeInactiveSearch()))
        {
            error = "Failed to wait for 'active/Inactive Search";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.activeInactiveSearch()))
        {
            error = "Failed to click on 'active/Inactive Search";
            return false;
        }
//        
//        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.activeInactiveSearch(), getData("Status")))
//      
//            {
//                error = "Failed to select Status checkbox :" + getData("Status");
//                return false;
//            }
//      
//         if (!SeleniumDriverInstance.pressEnter())
//            {
//                error = "Failed to press enter";
//                return false;
//            }
         
          pause(5000);
     if (!SeleniumDriverInstance.clickElementByJavascript(Inspection_Scheduler_PageObjects.statusSelect()))
            {
                error = "Failed to enter Status option :" + getData("Status");
                return false;
            }
 
    if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.statusDropClick()))
        {
            error = "Failed to click status drop close chevron ";
            return false;
        }
   narrator.stepPassedWithScreenShot("Status: " + getData("Status") + " -> " + getData("Status") + " -> " + getData("Status"));
 
       
        pause(5000);
        
//        //Click on search
         if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.searchButton()))
        {
            error = "Failed to click on 'search' button.";
            return false;
        }

       if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.searchButton()))
        {
            error = "Failed to click on 'Search' button.";
            return false;
        }
          
          narrator.stepPassedWithScreenShot("Successfully click 'search' button.");
          
          pause(8500);
          
          //Select record
          
      if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.clickRecord()))
        {
            error = "Failed to click on 'record'.";
            return false;
        }

       if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.clickRecord()))
        {
            error = "Failed to click on 'record' .";
            return false;
        }
          
          narrator.stepPassedWithScreenShot("Successfully click 'on the record' .");
          
          pause(5000);
          
            //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.InspectionScheduler_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
          //navigate to active/inactive dropdown
          
           if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.activeInactiveDD1()))
        {
            error = "Failed to wait for 'Inspection Scheduler' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.activeInactiveDD1()))
        {
            error = "Failed to click on 'Inspection Scheduler dropdown' module";
            return false;
        }
       
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Active/Inaction dropdown.");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.activeInactiveSearch()))
        {
            error = "Failed to wait for 'active/Inactive Search";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.activeInactiveSearch()))
        {
            error = "Failed to click on 'active/Inactive Search";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.activeInactiveSearch(), getData("Active/Inactive")))
      
            {
                error = "Failed to select Status checkbox :" + getData("Active/Inactive");
                return false;
            }
//      
//         if (!SeleniumDriverInstance.pressEnter())
//            {
//                error = "Failed to press enter";
//                return false;
//            }
//         
//          pause(5000);
     if (!SeleniumDriverInstance.clickElementByJavascript(Inspection_Scheduler_PageObjects.statusSelect1()))
            {
                error = "Failed to enter Status option :" + getData("Active/Inactive");
                return false;
            }

// 
   narrator.stepPassedWithScreenShot("Status: " + getData("Active/Inactive") + " -> " + getData("Active/Inactive") + " -> " + getData("Active/Inactive"));
pause(5000);
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_SaveDropButton1()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_SaveDropButton1()))
        {
            error = "Failed to click on Save Button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the save drop click");
        
        pause(4000);
        
        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_SaveCloseButton1()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_SaveCloseButton1()))
        {
            error = "Failed to click on Save Button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Save and Close");
        
        pause(5000);

return true;

}

}